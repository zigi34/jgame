package org.zigi.programming.game.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable {
	private static final long serialVersionUID = 2564365052283316051L;

	private static final int PANEL_WIDTH = 500;
	private static final int PANEL_HEIGHT = 400;

	private Thread animator;
	private volatile boolean running = false;
	private volatile boolean gameOver = false;
	private long period = 25;

	private Graphics dbg;
	private Image dbImage = null;

	public GamePanel() {
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

		setFocusable(true);

		requestFocus();
		readyForTermination();

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				testPress(e.getX(), e.getY());
			}
		});
	}

	private void readyForTermination() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int keyCode = e.getKeyCode();
				if ((keyCode == KeyEvent.VK_ESCAPE)
						|| (keyCode == KeyEvent.VK_Q)
						|| (keyCode == KeyEvent.VK_END)
						|| (keyCode == KeyEvent.VK_C) && e.isControlDown())
					running = false;
			}
		});
	}

	private void testPress(int x, int y) {
		if (!gameOver) {

		}
	}

	@Override
	public void run() {
		long beforeTime, timeDiff, sleepTime;

		beforeTime = System.currentTimeMillis();

		running = true;
		while (running) {
			gameUpdate();
			gameRender();
			paintScreen();

			timeDiff = System.currentTimeMillis() - beforeTime;

			sleepTime = period - timeDiff;
			if (sleepTime <= 0)
				sleepTime = 5;
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException ie) {

			}
			beforeTime = System.currentTimeMillis();
		}
		System.exit(0);
	}

	private void paintScreen() {
		Graphics g;
		try {
			g = this.getGraphics();
			if ((g != null) && (dbImage != null))
				g.drawImage(dbImage, 0, 0, null);

			Toolkit.getDefaultToolkit().sync();
			g.dispose();
		} catch (Exception e) {
			System.out.println("Chyba p�i pr�ci s grafick�m kontextem! "
					+ e.getMessage());
		}
	}

	@Override
	public void addNotify() {
		super.addNotify();
		startGame();
	}

	private void startGame() {
		if (animator == null || !running) {
			animator = new Thread(this);
			animator.start();
		}
	}

	public void stopGame() {
		running = false;
	}

	private void gameUpdate()
	{
		if(!gameOver)
			null;
	}

	private void gameRender() {
		if (dbImage == null) {
			dbImage = createImage(PANEL_WIDTH, PANEL_HEIGHT);
			if (dbImage == null) {
				System.out.println("dbImage m� hodnotu null");
				return;
			} else
				dbg = dbImage.getGraphics();
		}

		dbg.setColor(Color.WHITE);
		dbg.fillRect(0, 0, PANEL_WIDTH, PANEL_HEIGHT);

		if (gameOver)
			gameOverMessage(dbg);
	}

	private void gameOverMessage(Graphics g) {
		g.drawString("Nic", 0, 0);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (dbImage != null)
			g.drawImage(dbImage, 0, 0, null);
	}

}
